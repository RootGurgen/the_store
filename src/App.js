import React, {Suspense} from 'react';
import {BrowserRouter, Switch, Route, NavLink} from "react-router-dom";
import HomePage from "./Pages/HomePage";
import ProductItemContainerWithRouter from "./Components/productPage/productContainer";

import Preloader from './Components/assets/preloader.svg';

const App = () => {
    window.addEventListener('scroll', (event) => {
        console.log(pageYOffset)
    });

    const scrollOff = () => {
        const yPosition = pageYOffset
        window.onscroll = () => {
            window.scrollTo(0, yPosition)
        };
    }

    const scrollOn = () => {
        window.onscroll = (e) => {
            return
        };
    }

    const backgroundPageScrollOff = () => {
        console.log(pageYOffset, 'testing now')
        window.addEventListener('click', scrollOff)
    }

    const backgroundPageScrollOn = () => {
        window.removeEventListener('click', scrollOff)
        window.addEventListener('click', scrollOn)
        setTimeout(() => {
            window.removeEventListener('click', scrollOn)
        }, 1000)
    }
    return (
        <BrowserRouter>
            <div className="container">
                <Switch>
                    <Route path={'/'} exact render={() => {
                        return (
                            <HomePage
                                backgroundPageScrollOn={backgroundPageScrollOn}
                                backgroundPageScrollOff={backgroundPageScrollOff}
                            />
                        )
                    }}/>
                    <Route path={'/about-us'}
                           render={() => <HomePage yPosition={689} backgroundPageScrollOn={backgroundPageScrollOn}
                                                   backgroundPageScrollOff={backgroundPageScrollOff}/>}/>
                    <Route path={'/our-works'}
                           render={() => <HomePage yPosition={689} backgroundPageScrollOn={backgroundPageScrollOn}
                                                   backgroundPageScrollOff={backgroundPageScrollOff}/>}/>
                    <Route path={'/contact-us'}
                           render={() => <HomePage yPosition={3088} backgroundPageScrollOn={backgroundPageScrollOn}
                                                   backgroundPageScrollOff={backgroundPageScrollOff}/>}/>

                    <Route path='/product-item/:carouselId/:carouselUrl/:sliderItemId' render={() => {
                        return (
                            <Suspense fallback={() => <img src={Preloader} alt="...Loading"/>}>
                                <ProductItemContainerWithRouter
                                    backgroundPageScrollOn={backgroundPageScrollOn}
                                    backgroundPageScrollOff={backgroundPageScrollOff}
                                />
                            </Suspense>
                        )
                    }}/>
                </Switch>
            </div>
        </BrowserRouter>
    );
}
export default App;