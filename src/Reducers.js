import axios from 'axios'
const CREATE_TASK = 'create-task/reducer/CREATE_TASK';
import LesserArmenia from './Components/assets/Archived Season/lesser-armenia-1.jpg';
import MelodiesOfTheLegacy from './Components/assets/Archived Season/melodies-of-the-legacy-1.jpg';
import TheLastBastion from './Components/assets/Archived Season/the-last-bastion-1.jpg';

const GET_ITEMS = 'GET_ITEMS';

const initialState = {
    archivedSlider: [
        {
            url: 'archivedSlider',
            sliderTitle: 'ARCHIVED SEASON',
            seasonName: 'Archive',
            id: 1,
            sliderItems: [
                {
                    id: 1,
                    name: 'Season "Lesser Armenia"',
                    description: 'Ended: January 2019',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia'
                },
                {
                    id: 2,
                    name: 'Season "The Last Bastion"',
                    description: 'Ended: March 2019',
                    img: [MelodiesOfTheLegacy, TheLastBastion],
                    url: 'LatBastion'
                },
                {
                    id: 3,
                    name: 'Season "Melodies"',
                    description: 'Ended: July 2019',
                    img: [TheLastBastion, LesserArmenia],
                    url: 'LegacyMelodies'
                },

                {
                    id: 4,
                    name: 'Season "Melodies"',
                    description: 'Ended: July 2019',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia1'
                },
                {
                    id: 5,
                    name: 'Season "The Last Bastion"',
                    description: 'Ended: March 2019',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia2'
                },
                {
                    id: 6,
                    name: 'Season "Lesser Armenia"',
                    description: 'Ended: January 2019',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia3'
                },

                {
                    id: 7,
                    name: 'Season "The Last Bastion"',
                    description: 'Ended: March 2019',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia4'
                },
                {
                    id: 8,
                    name: 'Season "Melodies "',
                    description: 'Ended: July 2019',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia5'
                },
                {
                    id: 9,
                    name: 'Season "Lesser Armenia"',
                    description: 'Ended: January 2019',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia6'
                },

                {
                    id: 10,
                    name: 'Season "The Last Bastion"',
                    description: 'Ended: January 2019',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia7'
                },
                {
                    id: 11,
                    name: 'Season "The Last Bastion"',
                    description: 'Ended: March 2019',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia8'
                },
            ]
        },
    ],
    armenianPatterns: [
        {
            url: 'armenianPatterns',
            sliderTitle: 'PATTERNS',
            seasonName: '“Sebastia Gospel”',
            sliderDescription: '"Lesser Armenia"',
            id: 2,
            text: `Miniatures are from the times of 11th century Greater Armenia. The patterns have been preserved to this day
             thanks to the works of the great calligrapher and miniaturist Grigor Akoretsi. For this collection we used 
             the fragments of his work on the Sebastian Gospel of 1066. The calligraphic writing of the Armenian Kingdom
              of Cilicia (1080-1375) was created based on these works. The Cilician school of miniature was especially 
              famous, whose followers became well-known masters on painting churches and for centuries, taught this craft
               other peoples.`
            ,
            sliderItems: [
                {
                    id: 1,
                    name: 'lorem ipsum "Lesser Armenia"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia'
                },
                {
                    id: 2,
                    name: 'lorem ipsum "The Last Bastion"',
                    img: [MelodiesOfTheLegacy, TheLastBastion],
                    url: 'LatBastion'
                },
                {
                    id: 3,
                    name: 'lorem ipsum "Melodies"',
                    img: [TheLastBastion, LesserArmenia],
                    url: 'LegacyMelodies'
                },

                {
                    id: 4,
                    name: 'lorem ipsum "Melodies"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia1'
                },
                {
                    id: 5,
                    name: 'lorem ipsum "The Last Bastion"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia2'
                },
                {
                    id: 6,
                    name: 'lorem ipsum "Lesser Armenia"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia3'
                },

                {
                    id: 7,
                    name: 'lorem ipsum "The Last Bastion"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia4'
                },
                {
                    id: 8,
                    name: 'lorem ipsum "Melodies "',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia5'
                },
                {
                    id: 9,
                    name: 'lorem ipsum "Lesser Armenia"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia6'
                },

                {
                    id: 10,
                    name: 'lorem ipsum "The Last Bastion"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia7'
                },
                {
                    id: 11,
                    name: 'lorem ipsum "The Last Bastion"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia8'
                },
            ]
        },
    ],
    armenianLands: [
        {
            url: 'armenianLands',
            sliderTitle: 'LANDS',
            seasonName: '“Winding path”',
            sliderDescription: '"The Last Bastion"',
            id: 3,
            text: `In this series, you see the mural of one of the most prominent Christian monuments of the 7th 
            century - Zvartnots Cathedral (from ancient Armenian literally means “Celestial Angels”). 
            The architecture of the cathedral had a tremendous impact on the masters of that period. 
            The Byzantine Emperor Constant II desired to build a similar cathedral in Constantinople. 
            The details of Zvartnots Cathedral architecture appeared on the monuments’ ornaments of different nations up
             to the 12th century. The architectural composition of those times was distinguished by an amazing constancy
              in the ornament motifs, persistently repeated in various combinations and structures. Remaining unchanged,
               the same subjects decorated the walls of churches and monuments in Armenia throughout the 7th century.`,
            sliderItems: [
                {
                    id: 1,
                    name: 'lorem ipsum "Lesser Armenia"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia'
                },
                {
                    id: 2,
                    name: 'lorem ipsum "The Last Bastion"',
                    img: [MelodiesOfTheLegacy, TheLastBastion],
                    url: 'LatBastion'
                },
                {
                    id: 3,
                    name: 'lorem ipsum "Melodies"',
                    img: [TheLastBastion, LesserArmenia],
                    url: 'LegacyMelodies'
                },

                {
                    id: 4,
                    name: 'lorem ipsum "Melodies"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia1'
                },
                {
                    id: 5,
                    name: 'lorem ipsum "The Last Bastion"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia2'
                },
                {
                    id: 6,
                    name: 'lorem ipsum "Lesser Armenia"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia3'
                },

                {
                    id: 7,
                    name: 'lorem ipsum "The Last Bastion"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia4'
                },
                {
                    id: 8,
                    name: 'lorem ipsum "Melodies "',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia5'
                },
                {
                    id: 9,
                    name: 'lorem ipsum "Lesser Armenia"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia6'
                },

                {
                    id: 10,
                    name: 'lorem ipsum "The Last Bastion"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia7'
                },
                {
                    id: 11,
                    name: 'lorem ipsum "The Last Bastion"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia8'
                },
            ]
        },
    ],
    armenianGeniuses: [
        {
            url: 'armenianGeniuses',
            sliderTitle: 'GENIUSES',
            seasonName: '“Eastern moments”',
            sliderDescription: '"Melodies of the Legacy"',
            id: 4,
            text: `This collection is dedicated to a great artist of the 20th century - Martiros Saryan.  In this collection, 
            you see details of the works of the great artist. As you know, the main subjects of Saryan’s art were 
            associated with the nature and culture of Armenia, however, the details on the bags of this season refer to 
            the period of Saryan 1910 - 1913, when he, having made a number of trips to Turkey, Egypt and Iran, painted 
            pictures of oriental themes. Saryan himself, recalling this period of his life: “I had a goal - to understand
             the East, to find its characteristic features in order to further substantiate my searches in painting.
               I wanted to convey the realism of the East, to find convincing ways of depicting of this world ... to 
               reveal its new artistic understanding. <...> "The greatest interest for me were streets, the rhythm of 
               their life, the bright crowds of people and dogs ... living here in whole family groups."`,
            sliderItems: [
                {
                    id: 1,
                    name: 'lorem ipsum "Lesser Armenia"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia'
                },
                {
                    id: 2,
                    name: 'lorem ipsum "The Last Bastion"',
                    img: [MelodiesOfTheLegacy, TheLastBastion],
                    url: 'LatBastion'
                },
                {
                    id: 3,
                    name: 'lorem ipsum "Melodies"',
                    img: [TheLastBastion, LesserArmenia],
                    url: 'LegacyMelodies'
                },

                {
                    id: 4,
                    name: 'lorem ipsum "Melodies"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia1'
                },
                {
                    id: 5,
                    name: 'lorem ipsum "The Last Bastion"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia2'
                },
                {
                    id: 6,
                    name: 'lorem ipsum "Lesser Armenia"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia3'
                },

                {
                    id: 7,
                    name: 'lorem ipsum "The Last Bastion"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia4'
                },
                {
                    id: 8,
                    name: 'lorem ipsum "Melodies "',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia5'
                },
                {
                    id: 9,
                    name: 'lorem ipsum "Lesser Armenia"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia6'
                },

                {
                    id: 10,
                    name: 'lorem ipsum "The Last Bastion"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia7'
                },
                {
                    id: 11,
                    name: 'lorem ipsum "The Last Bastion"',
                    img: [LesserArmenia, MelodiesOfTheLegacy],
                    url: 'LesserArmenia8'
                },
            ]
        },
    ],
    id: 0
}

export const ItemReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ITEMS:
            return {
                ...state,
                items: state.items
            }
        default:
            return state
    }
}


// const getItemsAC = () => ({type :GET_ITEMS});
//
// export const getItemsThunk = () => {
//     return async (dispatch) => {
//         await dispatch(getItemsAC())
//     }
// }
//
// export const postEmailThunk = (email) => {
//     return async () => {
//         await aboutPriceRequest.postEmail(email)
//     }
// }


//
// const initialState = {
//     tasks: [
//         {1: '233'},
//         {2: '234'},
//         {3: '235'},
//     ]
// }
//
// export const Reducer = (state=initialState,action) => {
//     switch (action.type) {
//         case CREATE_TASK:
//             return {
//                 ...state,
//                 tasks: action.task
//             }
//         default:
//             return state
//     }
// }
//
//
// export const getTasksThunk =  () => {
//     return async (dispatch) => {
//         const response = await axios.get('https://jsonplaceholder.typicode.com/todos')
//         console.log(response,'test')
//     }
// }
//
//
