import React, {useState, useEffect} from 'react';
import {Header} from "../Components/header/header";
import {AboutUs} from "../Components/about/about";
import {ContactUs} from "../Components/contactUs/contactUs";
import {Footer} from "../Components/footer/footer";
import {connect} from "react-redux";
import {ProductSliderContainer} from "../Components/productsSlider/productsSliderContainer";
import {HeaderSlider} from "../Components/headerSlider/headerSlider";
import {ContactingComponent} from "../Components/contactingComponent/contactingComponent";

const mapStateToProps = (store) => {
    return {
        archivedSlider: store.ItemReducer.archivedSlider,
        armenianPatterns: store.ItemReducer.armenianPatterns,
        armenianLands: store.ItemReducer.armenianLands,
        armenianGeniuses: store.ItemReducer.armenianGeniuses,
    }
}

const HomePage = (
    {archivedSlider,
        armenianPatterns,
        armenianLands,
        armenianGeniuses,
        backgroundPageScrollOn,
        backgroundPageScrollOff,
        yPosition
    }) => {

    const [contactingComponent, setContactingComponent] = useState(false)

    useEffect( () => {
        window.scrollTo({top: yPosition, behavior: 'smooth'})
    },[yPosition])
    return (

        <>
            <div>
                <Header color='#fff'/>
                <HeaderSlider/>

                <AboutUs/>
                <ProductSliderContainer db={armenianPatterns}/>
                <ProductSliderContainer db={armenianLands}/>
                <ProductSliderContainer db={armenianGeniuses}/>
                <ContactUs
                    backgroundPageScrollOff={backgroundPageScrollOff}
                    contactingComponent={contactingComponent}
                    setContactingComponent={setContactingComponent}
                />
                <Footer/>
                {
                    contactingComponent
                        ? <ContactingComponent
                            backgroundPageScrollOn={backgroundPageScrollOn}
                            contactingComponent={contactingComponent}
                            setContactingComponent={setContactingComponent}
                          />
                        : null
                }
            </div>
        </>
    )
}

export default connect(mapStateToProps, null)(HomePage)


