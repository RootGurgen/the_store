import {ItemReducer} from './Reducers'
import {combineReducers, createStore,applyMiddleware} from "redux";
import thunk from "redux-thunk";
import {composeWithDevTools} from "redux-devtools-extension";

const rootReducer = combineReducers(
    {
        ItemReducer
    }
);

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));
export default store