import React, {useState} from 'react';
import closeImg from '../assets/close.svg'
import {Textarea} from "../textarea/textarea";
import {ThankYouPopUp} from "../thankYouPopUp/thankYouPopUp";

export const ContactingComponent = ({contactingComponent, setContactingComponent, backgroundPageScrollOn}) => {

    const [thankYouPopUpHide, setThankYouPopUpHide] = useState(false)
    return (
        <>
            <div className='page-bg'>
                {
                    thankYouPopUpHide
                        ? <ThankYouPopUp contactingComponent={contactingComponent} setContactingComponent={setContactingComponent} backgroundPageScrollOn={backgroundPageScrollOn}/>
                        : <div className='contacting-component'>
                            <div className='title'>
                                <div className='contacting-component__header'>
                                    <div className='contacting-component__title'>CONTACTING NARIN</div>
                                    <img src={closeImg} alt="close" className='contacting-component__close'
                                         onClick={() => {
                                             setContactingComponent(!contactingComponent)
                                             backgroundPageScrollOn()
                                         }}/>
                                </div>
                            </div>
                            <div className='description contacting-component__description'>Doing business with us is always
                                a good idea.
                            </div>
                            <div className='contacting-component__label'>
                                <label htmlFor="contacting-input">Your email address*</label>
                            </div>
                            <input type="text" id='contacting-input' className='input contacting-component__input'
                                   placeholder='Enter your email address*'/>
                            <div className='contacting-component__textarea'>
                                <Textarea placeholder='Your text here'/>
                            </div>
                            <button className='btn contacting-component__btn'
                                    onClick={() => setThankYouPopUpHide(!thankYouPopUpHide)}>Submit
                            </button>
                        </div>
                }
            </div>

        </>
    )
}