import React, {useState} from 'react';

// images
import MobileLogo from './images/logo-slogan.png';
import BlackMobileLogo from './images/logo-slogan-black.svg';
import DesktopLogo from './images/Dekstop-logo.svg';
import DesktopBlackLogo from '../assets/DesktopBlackLogo.svg'
import {BurgerMenu, MenuLinks, MobileMenuLinks} from "../menu/menu";

export const Header = ({color}) => {

    const [ burgerMenuToggle, setBurgerMenuToggle ] = useState(true)

    return (
        <div className='header'>
            <div className="justify-center">
                <div className="header__top" style={ burgerMenuToggle ? {background: `transparent`} : {background: '#fff' }}>
                    <div className="header__logo">
                        <a href="/">
                            <>
                                {
                                    burgerMenuToggle && color === '#fff'
                                        ? <img src={MobileLogo} alt="Logo" className='header__logo--mobile'/>
                                        : <img src={BlackMobileLogo} alt="Logo" className='header__logo--mobile'/>
                                }
                            </>
                            <>
                                {
                                    color === '#fff'
                                        ? <img src={DesktopLogo} alt="Logo" className='header__logo--desktop'/>
                                        : <img src={DesktopBlackLogo} alt='Logo' className='header__black-logo--desktop'/>
                                }
                            </>
                        </a>
                    </div>
                    <div className="header__menu">
                        <MenuLinks color={color}/>
                    </div>
                    <div className="header__burger-menu">
                        <BurgerMenu burgerMenuToggle={burgerMenuToggle} setBurgerMenuToggle={setBurgerMenuToggle} color={color}/>
                    </div>
                    <div className="header__mobile-menu" style={burgerMenuToggle ? {display: 'none'} : {display: 'contents'}}>
                        <MobileMenuLinks burgerMenuToggle={burgerMenuToggle} setBurgerMenuToggle={setBurgerMenuToggle}/>
                    </div>
                </div>
            </div>
        </div>
    )
}

