import React from 'react';

export const Footer = () => {
    return(
        <div className='footer'>
            <div className='description'>© narin 2020. All rights reserved.</div>
        </div>
    )
}