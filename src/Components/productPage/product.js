import React from "react";

// IMG
import facebook from '../assets/facebook.svg';
import instagram from '../assets/instagram.svg';
import {Footer} from "../footer/footer";
import {Header} from "../header/header";
import Facebook from "../assets/facebook.svg";
import Instagram from "../assets/instagram.svg";
import {ProductPageSlider} from "../productPageSlider/productPageSlider";
import {PriceRequest} from "../priceRequest/priceRequest";

export const Product = ({ProductItem, ProductInformation, PackagingBoxes, SeasonText, backgroundPageScrollOff, backgroundPageScrollOn, priseRequestPopupHide, setPriseRequestPopupHide}) => {
    return (
        <div>
            <Header color='#070707'/>
            <div className='product'>
                <div className='product__header'>
                    <div className='product__header--slider'><ProductPageSlider/></div>
                    <div className='product__information'>
                        <div>
                            <h1 className='title product__information--title'>YERVANDUNI COAT OF ARMS</h1>
                            <div className='description'>
                                <div>Season "Lesser Armenia"</div>
                                <div>- Each product is limited from 3 to 9 pieces.</div>
                                <div>- We spend around 11 hours of handwork per product.</div>
                                <div>- Unique serial number could be found inside of the bag.</div>
                            </div>
                        </div>

                        <h1 className='title'>
                            <div className='product__information--title'>PRODUCT INFORMATION</div>
                        </h1>
                        <div>
                            {
                                ProductInformation.map(i => {
                                    return (
                                        <div key={i.id} className='product__information--wrap description'>
                                            <div>
                                                <div className='product__information--description'>{i.description}</div>
                                            </div>
                                            <div>
                                                <div className='product__information--value'>{i.descriptionValue}</div>
                                            </div>
                                        </div>
                                    )
                                })
                            }
                        </div>
                        <button className='btn product__information--btn' onClick={() => {
                                backgroundPageScrollOff()
                                setPriseRequestPopupHide()
                        }}>Request the price</button>
                        <div className='product__social'>
                            <div className='description'>Or write us directly via Facebook or Instagram</div>
                            <div className='social'>
                                <div className='social__img'>
                                    <img src={facebook} alt="facebook" className='social__img--facebook'/>
                                    <img src={instagram} alt="instagram"/>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>


                <div className='product__introduction'>
                    <h1 className='title product__introduction--title'>INTRODUCTION</h1>
                    <div className='product__introduction--wrap'>
                        <div className='description'>
                            {SeasonText}
                        </div>
                        <div className='product__introduction--iframe'>
                            <iframe src="https://www.youtube.com/embed/R5QECN9ae4k" frameBorder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen className='product__iframe'> </iframe>
                        </div>
                    </div>
                </div>

                <div className='product__packaging'>
                    <h1 className='title product__packaging--title'>PACKAGING</h1>
                    <div className='product__packaging--items'>
                        {
                            PackagingBoxes.map(i => {
                                return (
                                    <div key={i.id}>
                                        <div className='product__packaging--item'>

                                            <label className='description product__packaging--name'>
                                                <input type="radio" name='video' className='product__packaging--radio'/>
                                                {i.title}
                                            </label>
                                            <div className='description'>
                                                <div className='product__packaging--description'>{i.description}</div>
                                            </div>
                                            <div className='product__packaging--iframe'>
                                                <iframe src={i.videoSrc} className='product__iframe'> </iframe>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                    <div className='product__packaging--footer'>
                        <div className='description product__packaging--info'>All packages includes the product story color print</div>
                        <button className='btn product__packaging--btn'>Show the example</button>
                    </div>
                </div>

                <div className='product__buy'>
                    <h1 className='title'>WHERE TO BUY</h1>
                    <div className='description product__buy--description'>We ship worldwide via DHL (3-7 days)</div>
                    <button className='btn product__buy--btn' onClick={() => {
                        backgroundPageScrollOff()
                        setPriseRequestPopupHide()
                    } }>Request the price</button>
                    <div className='product__buy--social'>
                        <div className="description product__buy--text">Or write us directly on Facebook or Instagram</div>
                        <div className='social'>
                            <div className='social__img product__buy--social'>
                                <img src={Facebook} alt="facebook" className='social__img--facebook'/>
                                <img src={Instagram} alt="instagram"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>

            {
                priseRequestPopupHide
                    ? <PriceRequest
                        setPriseRequestPopupHide={setPriseRequestPopupHide}
                        backgroundPageScrollOn={backgroundPageScrollOn}
                      />

                    : null
            }
        </div>
    )
}