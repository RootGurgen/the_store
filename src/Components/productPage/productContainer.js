import React, {useState} from 'react'
import {withRouter} from "react-router-dom";
import {connect} from "react-redux";
import {Product} from "./product";
import {PriceRequest} from "../priceRequest/priceRequest";

const mapStateToProps = (store) => {
    return {
        seasons: store.ItemReducer
    }
}

class ProductItemContainer extends React.Component {

    state = {
        ProductInformation: [
            {id: 1, description: 'Product weight', descriptionValue: '1kg'},
            {id: 2, description: 'Shipping total weight', descriptionValue: '1.5kg'},
            {id: 3, description: 'Product dimension', descriptionValue: '52 x 14 x 52cm'},
            {id: 4, description: 'Materials', descriptionValue: 'Leather'},
            {id: 5, description: 'Min. stripe length', descriptionValue: '20cm'},
            {id: 6, description: 'Max. stripe length', descriptionValue: '30cm'}
        ],

        PackagingBoxes: [
            {
                id: 1,
                title: 'Standard',
                description: 'Elegant, high-quality, and eco-friendly box with durable magnets on the lid that provide a tight and safe seal, securing your bag inside.',
                videoSrc: "https://www.youtube.com/embed/08EgUyjSy7E"
            },
            {
                id: 2,
                title: 'Wooden Box',
                description: 'Crafted from Armenian wood this box is for the ones who want to make great impression.',
                videoSrc: "https://www.youtube.com/embed/TkTAb5yOHLs"
            },
            {
                id: 3,
                title: 'Wooden Box - Special Edition',
                description: 'Any name or message of your choice will be engraved on the front of the box.',
                videoSrc: "https://www.youtube.com/embed/jAh73Cnryjc"
            }
        ],

        priseRequestPopupHide: false
    }

    setPriseRequestPopupHide = () => {
        console.log('setPriseRequestPopupHide')
        this.setState({priseRequestPopupHide: !this.state.priseRequestPopupHide})
    }

    render() {

        console.log(this.props, 'this.props')

        const carouselId = this.props.match.params.carouselId
        const carouselUrl = this.props.match.params.carouselUrl
        const sliderItemId = this.props.match.params.sliderItemId

        const carousel = Object.values(this.props.seasons)[carouselId].map(i => i.sliderItems)
        const seasonText = Object.values(this.props.seasons)[carouselId].map(i => i.text)
        const theItem = carousel[0].filter(item => item.id === +sliderItemId)[0]
        console.log(seasonText, 'carousel')
        return (
            <>

                <Product
                    priseRequestPopupHide={this.state.priseRequestPopupHide}
                    setPriseRequestPopupHide={this.setPriseRequestPopupHide}
                    backgroundPageScrollOff={this.props.backgroundPageScrollOff}
                    backgroundPageScrollOn={this.props.backgroundPageScrollOn}
                    ProductItem={theItem}
                    SeasonText={seasonText}
                    ProductInformation={this.state.ProductInformation}
                    PackagingBoxes={this.state.PackagingBoxes}
                />


            </>
        )
    }

}

let ProductItemContainerWithRouter = withRouter(ProductItemContainer)
export default connect(mapStateToProps,null)(ProductItemContainerWithRouter)