import React, {useState} from 'react'
// Import Swiper React components
import {Swiper, SwiperSlide} from 'swiper/react';
import SwiperCore, {Navigation, Pagination, Scrollbar, A11y, Autoplay, Thumbs} from 'swiper';

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y, Autoplay, Thumbs]);
// Import Images

import LeftArrow from '../assets/left-arrow.svg'
import RightArrow from '../assets/right-arrow.svg'
// Import Swiper styles
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/scrollbar/scrollbar.scss';
import 'swiper/components/pagination/pagination.scss';


export const ProductPageSlider = () => {
    const [id, setId] = useState(0)
    const t = [
        {index: 0, color: '#e4e4e4'},
        {index: 1, color: 'skyBlue'},
        {index: 2, color: 'pink'},
        {index: 3, color: '#f8f8f8'},
        {index: 4, color: 'orange'}
    ]

    return (

        <div className='product-item-container'>

            <div className="mini-img-section">
                {t.map(item => {
                    return (
                        <div onClick={() => setId(item.index)}
                             key={item.index}
                             style={{backgroundColor: `${item.color}`}}
                             className='mini-img'
                        >
                        </div>
                    )
                })}
            </div>

            <div className='product-slider-container'>
                <div className="product-page__slider">
                    {t.map(i => <div className='hed-slid' key={i.index} style={id === i.index ? {
                        zIndex: '9',
                        backgroundColor: `${i.color}`
                    } : {display: 'none'}}>
                        <div className='arrows'>
                            <img src={LeftArrow} alt="Left Arrow" onClick={() => {
                                console.log(id, 'Id')
                                if (id < 1) {
                                    setId(4)
                                } else {
                                    setId(id - 1)
                                }
                            }}/>

                            <img src={RightArrow} alt="Right Arrow" onClick={() => {
                                console.log(id, 'Id')
                                if (id > 3) {
                                    setId(0)
                                } else {
                                    setId(id + 1)
                                }
                            }}/>
                        </div>
                    </div>)}

                </div>

            </div>
        </div>
    )
}