import React from 'react';
import {NavLink} from "react-router-dom";

const smoothScroll = (yPosition, color) => {
    if (color === '#fff') {
        console.log(color, 'color')
        return window.scrollTo({top: yPosition, behavior: 'smooth'})

    }else {
        console.log('false', color)
    }
}

export const BurgerMenu = ({burgerMenuToggle, setBurgerMenuToggle, color}) => {

    return (
        <div className={`menu`}>
            {
                burgerMenuToggle
                    ? <div className="menu__lines" onClick={() => setBurgerMenuToggle(!burgerMenuToggle)}>
                        <div className="menu__line menu__line--first-line" style={burgerMenuToggle && color === '#fff' ? {borderColor: '#fff'} : {borderColor: '#222'}}> </div>
                        <div className="menu__line menu__line--second-line" style={burgerMenuToggle && color === '#fff' ?{borderColor: '#fff'} : {borderColor: '#222'}}> </div>
                        <div className="menu__line menu__line--third-line" style={burgerMenuToggle && color === '#fff' ? {borderColor: '#fff'} : {borderColor: '#222'}}> </div>
                      </div>

                    : <>
                        <div className="menu__lines" onClick={() => setBurgerMenuToggle(!burgerMenuToggle)}>
                            <div className="menu__line menu__line--close-first"> </div>
                            <div className="menu__line menu__line--close-second"> </div>
                            <div className="menu__line menu__line--close-third"> </div>
                        </div>

                      </>
            }
        </div>
    )
}

export const MenuLinks = ({color}) => {
    return (
        <>
            <div className="menu__items description" style={{color: `${color}`}}>
                <NavLink
                    to='/about-us'
                    className="menu__item"
                    onClick={() => smoothScroll(689, color)}
                    target={color !== '#fff' ? "_blank" : "_self"}
                >
                    About us
                </NavLink>

                <NavLink
                    to='/our-works'
                    className="menu__item"
                    onClick={() => smoothScroll(689, color)}
                    target={color !== '#fff' ? "_blank" : "_self"}
                >
                    Our works
                </NavLink>

                <NavLink
                    to='/contact-us'
                    className="menu__item"
                    onClick={() => smoothScroll(3088, color)}
                    target={color !== '#fff' ? "_blank" : "_self"}
                >
                    Contacts us
                </NavLink>
            </div>
        </>
    )
}

export const MobileMenuLinks = () => {
    return (
        <>
            <div className="menu__items description">
                <div className="menu__item">About us</div>
                <div className="menu__item">Our works</div>
                <div className="menu__item">Contacts us</div>
            </div>
        </>
    )
}