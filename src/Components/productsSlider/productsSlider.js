import React from 'react';
// import Swiper core and required components
import SwiperCore, {Navigation, Pagination, Scrollbar, A11y} from 'swiper';

import {Swiper, SwiperSlide} from 'swiper/react';

// Import Swiper styles
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import 'swiper/components/scrollbar/scrollbar.scss';
import {NavLink} from "react-router-dom";

// install Swiper components
SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

export const ProductSlider = ({collection}) => {

    console.log(collection, 'collection')
    return (
        <div>
            <div className='product-slider'>
                <h1 className='product-slider--title'>{collection.sliderTitle}</h1>
                <div className='product-slider--description'>Season {collection.seasonName}</div>
                <div className='product-slider__wrap'>
                    <Swiper
                        slidesPerView={1.2}
                        spaceBetween={16}
                        navigation
                        breakpoints={{
                            580: {
                                spaceBetween: 16,
                                slidesPerView: 2,
                            },

                            1024: {
                                spaceBetween: 30,
                                slidesPerView: 3,
                            },
                        }}
                        onSwiper={(swiper) => console.log(swiper)}
                        onSlideChange={() => console.log('slide change')}
                    >
                        {
                            collection.sliderItems.map(i => {
                                return (
                                    <SwiperSlide className="product-slider__item" key={i.id}>
                                        <NavLink to={`/product-item/${collection.id - 1}/${collection.url}/${i.id}`} aria-label={collection.id}>
                                            <div>
                                                <img src={i.img[0]} alt="bags" className='product-slider__item--img'/>
                                                <div className='description'>
                                                    <div className='product-slider__item--description'>{i.name}</div>
                                                </div>
                                            </div>
                                        </NavLink>
                                    </SwiperSlide>
                                )
                            })
                        }
                    </Swiper>
                </div>
            </div>
        </div>
    )
}

