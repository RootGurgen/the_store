import React from 'react'
import {ProductSlider} from "./productsSlider";

export const ProductSliderContainer = (db) => {
    const collection = Object.values(db)[0].map(i => i)[0]

    return(
        <>
         <ProductSlider collection={collection}/>
        </>
    )
}