import React from "react";

export const Textarea = ({placeholder, height}) => {
    return(
        <>
            <textarea className='textarea' placeholder={placeholder}></textarea>
        </>
    )
}