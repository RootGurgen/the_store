import React from 'react'
// Import Swiper React components
import {Swiper, SwiperSlide} from 'swiper/react';
import SwiperCore, {Navigation, Pagination, Scrollbar, A11y, Autoplay} from 'swiper';

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y, Autoplay]);
// Import Images
import Slider_Img1 from './Images/bag__1.jpg';
import Slider_Img2 from './Images/bag__2.jpg';
import Slider_Img3 from './Images/bag__3.jpg';
import Slider_Img4 from './Images/bag__4.jpg';
// Import Swiper styles
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import 'swiper/components/scrollbar/scrollbar.scss';

export const HeaderSlider = () => {
    const slide = [Slider_Img1, Slider_Img2, Slider_Img3, Slider_Img4]
    return (
        <Swiper
            spaceBetween={0}
            slidesPerView={1}
            speed={500}
            pagination={{ clickable: true }}
            loop
            navigation = {
                {
                    hideOnClick: false
                }
            }
            autoplay={ {
                delay: 5000,
            }}
            breakpoints={{
                640: {
                    spaceBetween: 0,
                    slidesPerView: 1,
                    autoplay: false,
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev'
                    }
                },

                1024: {
                    slidesPerView: 1,
                    autoplay: {
                        delay: 9000
                    },


                }
            }}
            onSlideChange={() => console.log('slide change')}
            onSwiper={(swiper) => console.log(swiper)}
        >
            {slide.map((i, index) => {
                return (
                    <SwiperSlide className='slider' key={index}>
                        <div style={{backgroundImage: `url('${i}')`}} className='slider__img'> </div>
                    </SwiperSlide>
                )
            })}

        </Swiper>
    );
};